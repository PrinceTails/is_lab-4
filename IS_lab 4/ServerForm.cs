﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace IS_lab_4
{
    public partial class ServerForm : Form
    {
        Socket sListener;
        IPEndPoint ipEndPoint;
        Diffie_Hellman dh;
        Socket handler;
        public ServerForm()
        {
            InitializeComponent();

            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            ipEndPoint = new IPEndPoint(ipAddr, 11000);

            sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            sListener.Bind(ipEndPoint);


        }
        private void server()
        {
            this.Controls.Add(new ListBox());
            
        }

        private void launch_server_Click(object sender, EventArgs e)
        {
            dh = new Diffie_Hellman();
            string message="";
            Random r = new Random();
            dh.a = r.Next(2, 10);
            dh.g = Convert.ToInt32(gValue.Value);
            dh.p = Convert.ToInt32(pValue.Value);
            dh.calculate_A();
            byte[] receiver = new byte[512];

            try
            {
                sListener.Listen(10);

                while (true)
                {
                    log.Items.Add(DateTime.Now.ToString()+" Сервер запущен. Ожидание подключения...");

                    handler = sListener.Accept();
                    log.Items.Add(DateTime.Now.ToString() + " Клиент присоединился.");
                    log.Items.Add(DateTime.Now.ToString() + " Вычислил случайное значение a = " + dh.a);
                    message += "g "+Convert.ToInt32(gValue.Value) + " p " + Convert.ToInt32(pValue.Value) + " A " + dh.A.ToString();
                    handler.Send(Encoding.UTF8.GetBytes(message));
                    log.Items.Add(DateTime.Now.ToString() + " Отправил клиенту значения g,p и A = " + dh.A.ToString());

                    int countByte = handler.Receive(receiver);
                    message = Encoding.UTF8.GetString(receiver, 0, countByte);
                    var str = message.Split(' ');
                    if(str[0]=="B")
                    {
                        dh.B = Convert.ToInt32(str[1]);
                        log.Items.Add(DateTime.Now.ToString() + " Получил от клиента число B = " + dh.B.ToString());
                        dh.calculate_K_server();
                        log.Items.Add(DateTime.Now.ToString() + " Вычислил ключ K = " + dh.K.ToString()); 
                    }
                    log.Items.Add(DateTime.Now.ToString() + " Обмен завершен. Клиент отключен. Сервер отключен.");
                    log.Items.Add("___________________________________________");
                    return;
                }
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.Message);
            }

        }

        private void ServerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(handler!=null)
                handler.Close();
        }
    }
}
