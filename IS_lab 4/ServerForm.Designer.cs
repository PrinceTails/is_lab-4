﻿namespace IS_lab_4
{
    partial class ServerForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.log = new System.Windows.Forms.ListBox();
            this.gValue = new System.Windows.Forms.NumericUpDown();
            this.pValue = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.launch_server = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pValue)).BeginInit();
            this.SuspendLayout();
            // 
            // log
            // 
            this.log.FormattingEnabled = true;
            this.log.Location = new System.Drawing.Point(12, 68);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(431, 238);
            this.log.TabIndex = 0;
            // 
            // gValue
            // 
            this.gValue.Location = new System.Drawing.Point(12, 35);
            this.gValue.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.gValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.gValue.Name = "gValue";
            this.gValue.Size = new System.Drawing.Size(120, 20);
            this.gValue.TabIndex = 1;
            this.gValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pValue
            // 
            this.pValue.Location = new System.Drawing.Point(194, 35);
            this.pValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.pValue.Name = "pValue";
            this.pValue.Size = new System.Drawing.Size(120, 20);
            this.pValue.TabIndex = 2;
            this.pValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Число g";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Число p";
            // 
            // launch_server
            // 
            this.launch_server.Location = new System.Drawing.Point(361, 13);
            this.launch_server.Name = "launch_server";
            this.launch_server.Size = new System.Drawing.Size(82, 42);
            this.launch_server.TabIndex = 5;
            this.launch_server.Text = "ЗАПУСТИТЬ";
            this.launch_server.UseVisualStyleBackColor = true;
            this.launch_server.Click += new System.EventHandler(this.launch_server_Click);
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 318);
            this.Controls.Add(this.launch_server);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pValue);
            this.Controls.Add(this.gValue);
            this.Controls.Add(this.log);
            this.Name = "ServerForm";
            this.Text = "Обмен ключами Диффи-Хелман. Сервер";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServerForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox log;
        private System.Windows.Forms.NumericUpDown gValue;
        private System.Windows.Forms.NumericUpDown pValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button launch_server;
    }
}

