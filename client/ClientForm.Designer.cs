﻿namespace client
{
    partial class ClientForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.launchClient = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // launchClient
            // 
            this.launchClient.Location = new System.Drawing.Point(13, 12);
            this.launchClient.Name = "launchClient";
            this.launchClient.Size = new System.Drawing.Size(115, 23);
            this.launchClient.TabIndex = 0;
            this.launchClient.Text = "Подключиться";
            this.launchClient.UseVisualStyleBackColor = true;
            this.launchClient.Click += new System.EventHandler(this.launchClient_Click);
            // 
            // log
            // 
            this.log.FormattingEnabled = true;
            this.log.Location = new System.Drawing.Point(13, 47);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(478, 199);
            this.log.TabIndex = 1;
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 261);
            this.Controls.Add(this.log);
            this.Controls.Add(this.launchClient);
            this.Name = "ClientForm";
            this.Text = "Обмен ключами Диффи-Хелман. Клиент";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button launchClient;
        private System.Windows.Forms.ListBox log;
    }
}

