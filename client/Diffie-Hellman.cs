﻿using System;

class Diffie_Hellman
    {
        public long a;
        public long K;
        public long A;
        public long g;
        public long p;
        public long B;

        public Diffie_Hellman()
        {
            a = K = A = g = p = B = 0;
        }

        public void calculate_A()
        {
            A = (long)Math.Pow(g, a) % p;
        }

        public void calculate_B()
        {
            B = (long)Math.Pow(g, a) % p;
        }
        public void calculate_K_client()
        {
            K = (long)Math.Pow(A, a) % p;
        }
        public void calculate_K_server()
        {
            K = (long)Math.Pow(B, a) % p;
        }
    }
