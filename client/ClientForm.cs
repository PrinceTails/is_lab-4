﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace client
{
    public partial class ClientForm : Form
    {
        IPEndPoint ipEndPoint;
        IPAddress ipAddr;
        Diffie_Hellman dh;
        public ClientForm()
        {
            InitializeComponent();
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            ipAddr = ipHost.AddressList[0];
            ipEndPoint = new IPEndPoint(ipAddr, 11000);
        }

        private void launchClient_Click(object sender, EventArgs e)
        {
            byte[] receiver = new byte[512];
            dh = new Diffie_Hellman();
            string message = "";
            Socket cSender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            cSender.Connect(ipEndPoint);
            if (cSender.Connected)
            {
                log.Items.Add(DateTime.Now.ToString() + " Подключился к серверу. Ожидаю данные...");
                int countByte = cSender.Receive(receiver);
                message = Encoding.UTF8.GetString(receiver, 0, countByte);
                var str = message.Split(' ');
                if(str[0]=="g")
                {
                    Random r = new Random();
                    dh.a = r.Next(2, 10);
                    dh.g = Convert.ToInt32(str[1]);
                    dh.p = Convert.ToInt32(str[3]);
                    dh.A = Convert.ToInt32(str[5]);
                    log.Items.Add(DateTime.Now.ToString() + " Получил от сервера значения g = " + dh.g + ", p = " + dh.p + ", A = " + dh.A);
                    log.Items.Add(DateTime.Now.ToString() + " Вычисленно случайное значение b = " + dh.a);

                    dh.calculate_B();
                    log.Items.Add(DateTime.Now.ToString() + " Вычислил и отправил серверу значение B = " + dh.B.ToString());
                    message = "B " + dh.B.ToString();
                    dh.calculate_K_client();
                    log.Items.Add(DateTime.Now.ToString() + " Вычислил значение K = " + dh.K.ToString());
                    cSender.Send(Encoding.UTF8.GetBytes(message));
                    log.Items.Add(DateTime.Now.ToString() + " Обмен завершен. Произведено отключение от сервера.");
                    log.Items.Add("___________________________________________");
                    cSender.Close();
                }
            }

        }
    }
}
